from django.conf import settings

from django.shortcuts import render
import stripe
from django.contrib.auth.decorators import login_required
# Create your views here.

stripe.api_key = settings.STRIPE_SECRET_KEY


@login_required
def checkout(request):
    publishkey = settings.STRIPE_PUBLISHABLE_KEY
    print(request.user.userstripe.stripe_id)
    if request.method == 'POST':
        token = request.POST['stripeToken']
        try:
            customer = stripe.Customer.create(description="Customer for jenny.rosen@example.com",)
            # customer = stripe.Customer.retreive("cus_GEYfFmQBqVxKnG")
            customer.sources.create(source=token)
            charge = stripe.Charge.create(
                amount=1000,
                currency='usd',
                description='Example charge',
                customer = customer,
            )
        except stripe.error.CardError as e:
            pass
    context = {'publishkey':publishkey}
    template = 'checkout.html'
    return render(request,template,context)
 
